package com.tcs.farmsssamplecollection.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tcs.farmsssamplecollection.dto.FarmerDTO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by harshdeepsingh on 29/03/17.
 */

// Database functions are defined in this activity
public class DBActivity {
    public static final String DATABASE_NAME = "farmss";
    public static final String DATABASE_TABLE = "farmers";
    public static final int DATABASE_VERSION = 1;
    public static final String KEY_ROWID = "_id";
    public static final String KEY_NAME = "name";
    public static final String KEY_MOBILE = "mobile";
    public static final String KEY_STATE = "state";
    public static final String KEY_DISTRICT = "district";
    public static final String KEY_TEHSIL = "tehsil";
    public static final String KEY_VILLAGE = "village";
    public static final String KEY_CODE = "pincode";
    public static final String KEY_PLOT_SIZE = "plotsize";
    public static final String KEY_CROP = "crop";
    public static final String KEY_LONGITUDE = "lat";
    public static final String KEY_LATITUDE = "long";
    public static final String KEY_UPDATE_STATUS = "updateStatus";
    public static final String KEY_REG_ID = "email";
    public static final String KEY_ADDED_ON = "added_on";
    private DBHelper dbHelper;
    private final Context myContext;
    private SQLiteDatabase myDataBase;

    class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        // Creating a sqlite table
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + DATABASE_TABLE +
                    "(" + KEY_ROWID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + KEY_NAME + " VARCHAR(100), "
                    + KEY_MOBILE + " VARCHAR(100), "
                    + KEY_STATE + " VARCHAR(100), "
                    + KEY_DISTRICT + " VARCHAR(100), "
                    + KEY_TEHSIL + " VARCHAR(100), "
                    + KEY_VILLAGE + " VARCHAR(100), "
                    + KEY_CODE + " VARCHAR(100), "
                    + KEY_PLOT_SIZE + " VARCHAR(100), "
                    + KEY_CROP + " VARCHAR(100), "
                    + KEY_LATITUDE + " VARCHAR(100), "
                    + KEY_LONGITUDE + " VARCHAR(100), "
                    + KEY_REG_ID + " VARCHAR(100), "
                    + KEY_ADDED_ON + " VARCHAR(100), "
                    + KEY_UPDATE_STATUS + " VARCHAR(100));");
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
            onCreate(db);
        }

    }

    public DBActivity(Context c) {
        myContext = c;
    }

    // close db
    public void close() {
        dbHelper.close();
    }

    // open database
    public DBActivity open() {
        dbHelper = new DBHelper(myContext);
        myDataBase = dbHelper.getWritableDatabase();
        return this;
    }

    // Insertion function
    public void insertDB(String name, String mobile, String state,
                         String district, String tehsil, String village,
                         String code, String plotsize, String crop, String latitude, String longitude, String email,
                         String addedOn) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_NAME, name);
        cv.put(KEY_MOBILE, mobile);
        cv.put(KEY_STATE, state);
        cv.put(KEY_DISTRICT, district);
        cv.put(KEY_TEHSIL, tehsil);
        cv.put(KEY_VILLAGE, village);
        cv.put(KEY_CODE, code);
        cv.put(KEY_PLOT_SIZE, plotsize);
        cv.put(KEY_CROP, crop);
        cv.put(KEY_LATITUDE, latitude);
        cv.put(KEY_LONGITUDE, longitude);
        cv.put(KEY_REG_ID, email);
        cv.put(KEY_ADDED_ON, addedOn);
        cv.put(KEY_UPDATE_STATUS, "no");
        myDataBase.insert(DATABASE_TABLE, null, cv);

    }

    /*public boolean checkFarmerPresence(String tableName, String mobileField, String mobileValue) {
        myDataBase = dbHelper.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + tableName + " where " + mobileField + " = " + mobileValue;
        Cursor cursor = myDataBase.rawQuery(selectQuery, null);
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }*/

    // Select query function
    public Cursor getData() {
        myDataBase = dbHelper.getReadableDatabase();

        String selectQuery = "SELECT  rowid as " +
                KEY_ROWID + "," +
                KEY_NAME + "," +
                KEY_MOBILE + "," +
                KEY_STATE + "," +
                KEY_DISTRICT + "," +
                KEY_TEHSIL + "," +
                KEY_VILLAGE + "," +
                KEY_CODE + "," +
                KEY_PLOT_SIZE + "," +
                KEY_CROP + "," +
                KEY_LATITUDE + "," +
                KEY_LONGITUDE + "," +
                KEY_REG_ID + "," +
                KEY_UPDATE_STATUS +
                " FROM " + DATABASE_TABLE;

        Cursor cursor = myDataBase.rawQuery(selectQuery, null);
        if (cursor == null) {
            return null;
        } else if (!cursor.moveToFirst()) {
            cursor.close();
            return null;
        }
        return cursor;
    }

    // obtaining farmer data
    public List<FarmerDTO> getFarmerData() {

        String selectQuery = "SELECT  rowid as " +
                KEY_ROWID + "," +
                KEY_NAME + "," +
                KEY_MOBILE + "," +
                KEY_STATE + "," +
                KEY_DISTRICT + "," +
                KEY_TEHSIL + "," +
                KEY_VILLAGE + "," +
                KEY_CODE + "," +
                KEY_PLOT_SIZE + "," +
                KEY_CROP + "," +
                KEY_LATITUDE + "," +
                KEY_LONGITUDE + "," +
                KEY_REG_ID + "," +
                KEY_ADDED_ON + "," +
                KEY_UPDATE_STATUS +
                " FROM " + DATABASE_TABLE;

        Cursor cursor = myDataBase.rawQuery(selectQuery, null);

        List<FarmerDTO> farmerDTOList = new ArrayList<FarmerDTO>();

        if (cursor.moveToFirst()) {
            do {
                farmerDTOList.add(new FarmerDTO(
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(6),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(7),
                        cursor.getString(8),
                        cursor.getString(9),
                        cursor.getString(12),
                        cursor.getString(13)
                ));
            } while (cursor.moveToNext());
        }
        return farmerDTOList;
    }

    public ArrayList<HashMap<String, String>> getAllFarmers() {
        ArrayList<HashMap<String, String>> farmerList = new ArrayList<HashMap<String, String>>();
        String selectQuery = "SELECT * FROM " + DATABASE_TABLE;
        myDataBase = dbHelper.getWritableDatabase();
        Cursor cursor = myDataBase.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                //map.put("_id", cursor.getString(0));
                map.put("name", cursor.getString(1));
                map.put("mobile", cursor.getString(2));
                map.put("state", cursor.getString(3));
                map.put("district", cursor.getString(4));
                map.put("tehsil", cursor.getString(5));
                map.put("village", cursor.getString(6));
                map.put("pincode", cursor.getString(7));
                map.put("plotsize", cursor.getString(8));
                map.put("crop", cursor.getString(9));
                map.put("lat", cursor.getString(10));
                map.put("long", cursor.getString(11));
                map.put("email", cursor.getString(12));
                map.put("addedOn", cursor.getString(13));
                farmerList.add(map);
            } while (cursor.moveToNext());
        }
        myDataBase.close();
        return farmerList;
    }

    public JSONArray composeJSONFromSQLite() throws JSONException {
//        ArrayList<HashMap<String, String>> farmerList = new ArrayList<HashMap<String, String>>();
        JSONArray jsonArray = new JSONArray();

        String selectQuery = "SELECT * FROM " + DATABASE_TABLE + " where " + KEY_UPDATE_STATUS + " = 'no'";
        myDataBase = dbHelper.getWritableDatabase();
        Cursor cursor = myDataBase.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
//                HashMap<String, String> map = new HashMap<String, String>();
                JSONObject jsonObject = new JSONObject();
                //map.put("_id", cursor.getString(0));
                jsonObject.put("name", cursor.getString(1));
                jsonObject.put("mobile", cursor.getString(2));
                jsonObject.put("state", cursor.getString(3));
                jsonObject.put("district", cursor.getString(4));
                jsonObject.put("tehsil", cursor.getString(5));
                jsonObject.put("village", cursor.getString(6));
                jsonObject.put("pincode", cursor.getString(7));
                jsonObject.put("plot_size", cursor.getString(8));
                jsonObject.put("crop", cursor.getString(9));
                jsonObject.put("lat", cursor.getString(10));
                jsonObject.put("long", cursor.getString(11));
                jsonObject.put("registered_by", cursor.getString(12));
                jsonObject.put("registered_on", cursor.getString(13));
                jsonArray.put(jsonObject);
            } while (cursor.moveToNext());
        }
        myDataBase.close();
        Gson gson = new GsonBuilder().create();
        return jsonArray;
    }

    public int dbSyncCount() {
        int count = 0;
        String selectQuery = "SELECT * FROM " + DATABASE_TABLE + " where " + KEY_UPDATE_STATUS + " = 'no'";
        myDataBase = dbHelper.getWritableDatabase();
        Cursor cursor = myDataBase.rawQuery(selectQuery, null);
        count = cursor.getCount();
        myDataBase.close();
        return count;
    }

    // the next two methods are used for checking the unsynced data by checking the value of the column named "update status".

    public String getSyncStatus() {
        String msg = null;
        if (this.dbSyncCount() == 0) {
            msg = "SQLite and Remote DBs are in Sync!";
        } else {
            msg = "DB Sync needed";
        }
        return msg;
    }

    public void updateSyncStatus(String status) {
        myDataBase = dbHelper.getWritableDatabase();
        String updateQuery = "UPDATE " + DATABASE_TABLE + " set " + KEY_UPDATE_STATUS + " = '" + status + "' where " + KEY_UPDATE_STATUS + " = 'no'";
        Log.d("query", updateQuery);
        myDataBase.execSQL(updateQuery);
        myDataBase.close();
    }
}
