package com.tcs.farmsssamplecollection.util;

/**
 * Created by harshdeepsingh on 29/03/17.
 */

public class EmailValidator {
    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

}
