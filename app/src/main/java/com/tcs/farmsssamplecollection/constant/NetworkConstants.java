package com.tcs.farmsssamplecollection.constant;

/**
 * Created by harshdeepsingh on 29/03/17.
 */

// Urls used in order to send and receive data
public interface NetworkConstants {

    String GET_NETWORK_IP = "http://farmss1-hoagsobjtech.rhcloud.com/";
    String GET_NETWORK_IP_PHP = "http://homephp.hol.es/";
    String LOGIN_URL = GET_NETWORK_IP + "/login";
    String REGISTRATION_URL = GET_NETWORK_IP + "/register";
    String RESET_PASSWORD_URL = GET_NETWORK_IP + "/resetpass";
    String RESET_PASSWORD_CHANGE_URL = RESET_PASSWORD_URL + "/chg";
    String GET_USER_URL = GET_NETWORK_IP + "/users";
    String ADD_FARMER_URL = GET_NETWORK_IP + "/addfarmer";
    String ADD_FARMER_SYNC_URL = GET_NETWORK_IP_PHP + "/sync.php";
    String GET_FARMER_URL = GET_NETWORK_IP_PHP + "/get_id.php";

}
