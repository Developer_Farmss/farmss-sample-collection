package com.tcs.farmsssamplecollection.dto;

/**
 * Created by harshdeepsingh on 05/04/17.
 */

public class CropDTO {
    String name;

    public CropDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
