package com.tcs.farmsssamplecollection.dto;

/**
 * Created by harshdeepsingh on 29/03/17.
 */

public class FarmerDTO {
    public String name;
    public String mobile;
    public String state;
    public String district;
    public String tehsil;
    public String village;
    public String pincode;
    public String plotsize;
    public String crop;
    public String email;
    public String addedOn;

    public FarmerDTO() {

    }

    public FarmerDTO(String name, String mobile, String village, String state,
                     String district, String tehsil, String pincode,
                     String plotsize, String crop, String email, String addedOn) {
        this.name = name;
        this.mobile = mobile;
        this.village = village;
        this.state = state;
        this.district = district;
        this.tehsil = tehsil;
        this.pincode = pincode;
        this.plotsize = plotsize;
        this.crop = crop;
        this.email = email;
        this.addedOn = addedOn;
    }

}
