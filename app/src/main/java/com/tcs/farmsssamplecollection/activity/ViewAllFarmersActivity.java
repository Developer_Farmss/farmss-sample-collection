package com.tcs.farmsssamplecollection.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.afollestad.materialdialogs.MaterialDialog;
import com.tcs.farmsssamplecollection.R;
import com.tcs.farmsssamplecollection.adapter.AllFarmersAdapter;
import com.tcs.farmsssamplecollection.dto.AllFarmersDTO;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by harshdeepsingh on 21/06/17.
 */

public class ViewAllFarmersActivity extends AppCompatActivity{

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.farmerList)
    RecyclerView farmerList;
    private AllFarmersAdapter adapter;
    private List<AllFarmersDTO> allFarmersDTOList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_all_farmers);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.view_all_farmers));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        allFarmersDTOList = new ArrayList<>();
        MaterialDialog dialog = new MaterialDialog.Builder(ViewAllFarmersActivity.this)
                .title(getString(R.string.fetching))
                .content(getString(R.string.please_wait))
                .progress(true, 0)
                .show();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
