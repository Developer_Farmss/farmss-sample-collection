package com.tcs.farmsssamplecollection.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

import com.tcs.farmsssamplecollection.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by harshdeepsingh on 10/05/17.
 */

// terms and conditions displayed in a webview
public class TermsActivity extends AppCompatActivity{

    @Bind(R.id.webViewTerms)
    WebView webViewTerms;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    String PDF_URL = "https://view.publitas.com/digital-impact-square/terms_and_conditions/";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_PROGRESS);
        setContentView(R.layout.activity_terms);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.terms_conditions));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getWindow().setFeatureInt( Window.FEATURE_PROGRESS, Window.PROGRESS_VISIBILITY_ON);
        webViewTerms.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress)
            {
                //Make the bar disappear after URL is loaded, and changes string to Loading...
                setTitle("Loading...");
                setProgress(progress * 100); //Make the bar disappear after URL is loaded

                // Return the app name after finish loading
                if(progress == 100)
                    setTitle(R.string.terms);
            }
        });
        webViewTerms.getSettings().setJavaScriptEnabled(true);
        webViewTerms.loadUrl(PDF_URL);
        webViewTerms.setHorizontalScrollBarEnabled(false);
        webViewTerms.getSettings().setSupportZoom(true);
        webViewTerms.getSettings().setBuiltInZoomControls(true);
        webViewTerms.getSettings().setDisplayZoomControls(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
