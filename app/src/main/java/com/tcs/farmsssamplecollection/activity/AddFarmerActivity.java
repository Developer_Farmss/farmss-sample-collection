package com.tcs.farmsssamplecollection.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.tcs.farmsssamplecollection.R;
import com.tcs.farmsssamplecollection.global.GPSTracker;
import com.tcs.farmsssamplecollection.ui.Snackbar;

import butterknife.Bind;
import butterknife.ButterKnife;
import fr.ganfra.materialspinner.MaterialSpinner;

/**
 * Created by harshdeepsingh on 29/03/17.
 */

//This is the activity which displays the form used for adding the farmer basic information.
public class AddFarmerActivity extends AppCompatActivity {

    //Declaring variables and simultaneously initializing them using ButterKnife library.
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.spinnerState)
    MaterialSpinner spinnerState;
    @Bind(R.id.spinnerDistrict)
    MaterialSpinner spinnerDistrict;
    @Bind(R.id.spinnerTehsil)
    MaterialSpinner spinnerTehsil;
    @Bind(R.id.btnAddFarmer)
    Button btnAddFarmer;
    @Bind(R.id.etFirstName)
    EditText etFirstName;
    @Bind(R.id.etLastName)
    EditText etLastName;
    @Bind(R.id.etMobileReg)
    EditText etMobileReg;
    @Bind(R.id.etPinCode)
    EditText etPinCode;
    @Bind(R.id.etVillage)
    EditText etVillage;
    @Bind(R.id.checkboxTerms)
    CheckBox checkboxTerms;
    private String[] state;
    private String[] district;
    private String[] tehsil;
    GPSTracker tracker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tracker = new GPSTracker(this);
        if (tracker.canGetLocation()) {

        } else {
            tracker.showSettingsAlert();
        }
        setContentView(R.layout.activity_add_farmer);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        //setting up the toolbar
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.add_new));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //hiding the keyboard during activity create.
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //using a custom typeface
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Whitney-Book-Bas.otf");
        etFirstName.setTypeface(typeface);
        etLastName.setTypeface(typeface);
        etMobileReg.setTypeface(typeface);
        etVillage.setTypeface(typeface);
        etPinCode.setTypeface(typeface);
        checkboxTerms.setTypeface(typeface);
        btnAddFarmer.setTypeface(typeface);

        //adding ripple effect to the buttons
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnAddFarmer.setBackgroundResource(R.drawable.ripple_rounded);
        }

        // populating the spinner
        state = getApplication().getResources().getStringArray(R.array.state);
        district = getApplication().getResources().getStringArray(R.array.district);
        tehsil = getApplication().getResources().getStringArray(R.array.tehsil);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddFarmerActivity.this, android.R.layout.simple_spinner_item, state);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerState.setAdapter(adapter);

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(AddFarmerActivity.this, android.R.layout.simple_spinner_item, district);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDistrict.setAdapter(adapter1);

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(AddFarmerActivity.this, android.R.layout.simple_spinner_item, tehsil);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTehsil.setAdapter(adapter2);

        btnAddFarmer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Validations
                if (etFirstName.getText().toString().equals("")) {
                    Snackbar.show(AddFarmerActivity.this, getString(R.string.farmer_name_error));
                    return;
                }
                if (etLastName.getText().toString().equals("")) {
                    Snackbar.show(AddFarmerActivity.this, getString(R.string.farmer_name_error));
                    return;
                }
                if (etMobileReg.getText().toString().equals("") || !(etMobileReg.getText().length() == 10)) {
                    Snackbar.show(AddFarmerActivity.this, getString(R.string.farmer_mobile_error));
                    return;
                }
                if (spinnerState.getSelectedItem().toString().equals(getString(R.string.select_state))) {
                    Snackbar.show(AddFarmerActivity.this, getString(R.string.farmer_state_error));
                    return;
                }
                if (spinnerDistrict.getSelectedItem().toString().equals(getString(R.string.select_district))) {
                    Snackbar.show(AddFarmerActivity.this, getString(R.string.farmer_district_error));
                    return;
                }
                if (spinnerTehsil.getSelectedItem().toString().equals(getString(R.string.select_tehsil))) {
                    Snackbar.show(AddFarmerActivity.this, getString(R.string.farmer_tehsil_error));
                    return;
                }
                if (etVillage.getText().toString().equals("")) {
                    Snackbar.show(AddFarmerActivity.this, getString(R.string.farmer_village_error));
                    return;
                }
                if (etPinCode.getText().toString().equals("") || !(etPinCode.getText().length() == 6)) {
                    Snackbar.show(AddFarmerActivity.this, getString(R.string.farmer_pincode_error));
                    return;
                }
                if (!checkboxTerms.isChecked()) {
                    Snackbar.show(AddFarmerActivity.this, getString(R.string.terms_and_conditions__error));
                    return;
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putString("farmerName", etFirstName.getText().toString() + " " + etLastName.getText().toString());
                    bundle.putString("farmerMobile", etMobileReg.getText().toString());
                    bundle.putString("state", spinnerState.getSelectedItem().toString());
                    bundle.putString("district", spinnerDistrict.getSelectedItem().toString());
                    bundle.putString("tehsil", spinnerTehsil.getSelectedItem().toString());
                    bundle.putString("village", etVillage.getText().toString());
                    bundle.putString("pincode", etPinCode.getText().toString());
                    Intent intent = new Intent(AddFarmerActivity.this, PlotAndCropActivity.class);
                    intent.putExtra("farmer", bundle);
                    startActivity(intent);

                    /*DBActivity db = new DBActivity(AddFarmerActivity.this);
                    db.open();
                    if (!db.checkFarmerPresence(DBActivity.DATABASE_TABLE, DBActivity.KEY_MOBILE, etMobileReg.getText().toString())) {
                        db.insertDB(etName.getText().toString(), etMobileReg.getText().toString(),
                                spinnerState.getSelectedItem().toString(), spinnerDistrict.getSelectedItem().toString(),
                                spinnerTehsil.getSelectedItem().toString(), etVillage.getText().toString(), etPinCode.getText().toString());
                        db.close();
                        Intent intent = new Intent(AddFarmerActivity.this, DashboardActivity.class);
                        intent.putExtra("farmer added", "success");
                        startActivity(intent);
                        finish();
                    } else {
                        Intent intent = new Intent(AddFarmerActivity.this, DashboardActivity.class);
                        intent.putExtra("farmer added", "failed");
                        startActivity(intent);
                        finish();
                    }*/
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            new MaterialDialog.Builder(AddFarmerActivity.this)
                    .title(R.string.confirmation)
                    .content(R.string.confirm_exit)
                    .positiveText(R.string.go_back)
                    .negativeText(R.string.cancel)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            onBackPressed();
                            finish();
                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                        }
                    })
                    .typeface("Whitney-Semibold-Bas.otf", "Whitney-Semibold-Bas.otf")
                    .show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        new MaterialDialog.Builder(AddFarmerActivity.this)
                .title(R.string.confirmation)
                .content(R.string.confirm_exit)
                .positiveText(R.string.go_back)
                .negativeText(R.string.cancel)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        finish();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .typeface("Whitney-Semibold-Bas.otf", "Whitney-Semibold-Bas.otf")
                .show();
    }
}
