package com.tcs.farmsssamplecollection.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.SearchView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.tcs.farmsssamplecollection.R;
import com.tcs.farmsssamplecollection.adapter.DashboardAdapter;
import com.tcs.farmsssamplecollection.constant.NetworkConstants;
import com.tcs.farmsssamplecollection.database.DBActivity;
import com.tcs.farmsssamplecollection.dto.FarmerDTO;
import com.tcs.farmsssamplecollection.global.GPSTracker;
import com.tcs.farmsssamplecollection.global.MainApplication;
import com.tcs.farmsssamplecollection.ui.Snackbar;
import com.tcs.farmsssamplecollection.util.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by harshdeepsingh on 29/03/17.
 */

// This is the home page of the application which displays the farmer's list present in the local database.
public class DashboardActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.fabAddFarmer)
    FloatingActionButton fabAddFarmer;
    @Bind(R.id.recyclerBlank)
    RecyclerView recyclerBlank;
    @Bind(R.id.searchView)
    SearchView searchView;
    private DashboardAdapter adapter;
    List<FarmerDTO> farmerDTOList;
    private List<FarmerDTO> items;
    private MaterialDialog dialog;
    private SessionManager sessionManager;
    private SharedPreferences preferences;
    DBActivity db;
    GPSTracker tracker;
    private static final String TAG = DashboardActivity.class.getSimpleName();

    //Butterknife library used to handle on click of the button
    @OnClick(R.id.fabAddFarmer)
    void addFarmer() {
        startActivity(new Intent(this, AddFarmerActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        tracker = new GPSTracker(this);
        if (tracker.canGetLocation()) {

        } else {
            //tracker.showSettingsAlert();
        }
        preferences = getSharedPreferences("Farmss", Context.MODE_PRIVATE);
        sessionManager = new SessionManager(getApplicationContext());
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.app_name));
        getSupportActionBar().setHomeButtonEnabled(true);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //fetching list from local database
        db = new DBActivity(this);
        db.open();
        farmerDTOList = db.getFarmerData();
        db.close();
        if (farmerDTOList == null) {
            Snackbar.show(DashboardActivity.this, getString(R.string.no_data_present));
        }

        //setting up search of the farmers in the searchview
        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint(getString(R.string.search_farmer));
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filter(newText);
                return false;
            }
        });

        // Initializing the adapter
        adapter = new DashboardAdapter(this, farmerDTOList, new DashboardAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(FarmerDTO item) {
                fetchFarmerData(item.email, item.addedOn);
            }
        });
        recyclerBlank.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);
        recyclerBlank.setLayoutManager(layoutManager);
    }

    private void fetchFarmerData(final String email, final String addedOn) {
        dialog = new MaterialDialog.Builder(DashboardActivity.this)
                .title(getString(R.string.please_wait))
                .content(getString(R.string.fetching))
                .progress(true, 0)
                .show();

        StringRequest request = new StringRequest(Request.Method.POST, NetworkConstants.GET_FARMER_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("Response:::", response);
                dialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int success = jsonObject.getInt("success");
                    if (success == 1) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("user");
                        Intent intent = new Intent(DashboardActivity.this, FarmerProfileActivity.class);
                        intent.putExtra("user", jsonObject1.toString());
                        startActivity(intent);
                    } else {
                        Snackbar.show(DashboardActivity.this, getString(R.string.sync_needed));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Snackbar.show(DashboardActivity.this, "Sync Needed!!!");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error:::", error.toString());
                dialog.dismiss();
                Snackbar.show(DashboardActivity.this, getString(R.string.network_error));
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("registered_on", addedOn);
                params.put("registered_by", email);
                return params;
            }
        };
        MainApplication.getInstance().addToRequestQueue(request);
    }

    void filter(String text) {
        List<FarmerDTO> temp = new ArrayList<>();
        for (FarmerDTO f : farmerDTOList) {
            if (f.name.toLowerCase().contains(text)) {
                temp.add(f);
            }
        }
        adapter.setFilter(temp);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        // Hanlding menu cases
        switch (id) {
            case R.id.logout:
                logout();
                break;
            case R.id.terms:
                startActivity(new Intent(this, TermsActivity.class));
                break;
            case R.id.about:
                startActivity(new Intent(this, AboutActivity.class));
                break;
            case R.id.sync:
                ArrayList<HashMap<String, String>> farmerList = db.getAllFarmers();
                if (farmerList.size() != 0) {
                    int t = db.dbSyncCount();
                    if (t != 0) {
                        syncSQLite();
                    } else {
                        Snackbar.show(DashboardActivity.this, getString(R.string.data_already_synced));
                    }
                } else {
                    Snackbar.show(DashboardActivity.this, getString(R.string.no_data_to_sync));
                }
        }
        return true;
    }


    // this method is responsible for sending the data to the server from the sqlite database to sql database.
    private void syncSQLite() {

        String tag_str_req = "req_sync";

        dialog = new MaterialDialog.Builder(DashboardActivity.this)
                .title(R.string.sync)
                .content(R.string.sync_message)
                .progress(true, 0)
                .show();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("farmer", db.composeJSONFromSQLite());
            Log.i("params:::", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(NetworkConstants.ADD_FARMER_SYNC_URL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                dialog.dismiss();
                Log.i(TAG, response.toString());
                try {
                    int res = response.getInt("success");
                    if (res == 1) {
                        db.updateSyncStatus("yes");
                        Snackbar.success(DashboardActivity.this, getString(R.string.sync_success));
                    } else {
                        Log.e(TAG, "Error");
                        Snackbar.show(DashboardActivity.this, getString(R.string.error_message));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Log.e(TAG, "Volley Error: " + error.getMessage());
                Snackbar.show(DashboardActivity.this, getString(R.string.network_error));
            }
        });
        MainApplication.getInstance().addToRequestQueue(jsonObjectRequest, tag_str_req);
    }

    private void logout() {
        sessionManager.setLogin(false);
        preferences.edit().clear().commit();
        Intent intent = new Intent(DashboardActivity.this, LoginActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("logout", "logout");
        intent.putExtras(bundle);
        startActivity(intent);
        ActivityCompat.finishAffinity(this);
    }
}
