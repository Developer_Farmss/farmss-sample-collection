package com.tcs.farmsssamplecollection.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tcs.farmsssamplecollection.R;
import com.tcs.farmsssamplecollection.constant.NetworkConstants;
import com.tcs.farmsssamplecollection.global.MainApplication;
import com.tcs.farmsssamplecollection.ui.Snackbar;
import com.tcs.farmsssamplecollection.util.EmailValidator;
import com.tcs.farmsssamplecollection.util.NetworkCheck;
import com.tcs.farmsssamplecollection.util.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by harshdeepsingh on 29/03/17.
 */

// Login page activity
public class LoginActivity extends AppCompatActivity {

    @Bind(R.id.etEmailRegister)
    EditText etEmail;
    @Bind(R.id.etPassword)
    EditText etPassword;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.btnLogin)
    Button btnLogin;
    @Bind(R.id.txtForgotPassword)
    TextView txtForgotPassword;
    @Bind(R.id.txtRegister)
    TextView txtRegister;
    @Bind(R.id.txtChangeLanguage)
    TextView txtChangeLanguage;
    private static final String TAG = LoginActivity.class.getSimpleName();
    private MaterialDialog dialog;
    private SessionManager sessionManager;
    private SharedPreferences preferences;

    @OnClick(R.id.btnLogin)
    void enter() {
        if (etEmail.getText().toString().equals("") || etPassword.getText().toString().equals("")) {
            Snackbar.show(this, getString(R.string.no_text));
            return;
        } else if(!EmailValidator.isValidEmail(etEmail.getText().toString())) {
            Snackbar.show(this, getString(R.string.invalid_email));
        } else if (!NetworkCheck.isNetworkAvailable(this)) {
            Snackbar.show(this, getString(R.string.no_internet));
        } else {
            login(etEmail.getText().toString(), etPassword.getText().toString());
        }
    }

    @OnClick(R.id.txtForgotPassword)
    void forgotPassword() {
        startActivity(new Intent(this, ForgotPasswordActivity.class));
    }

    @OnClick(R.id.txtChangeLanguage)
    void changeLanguage() {
        startActivity(new Intent(this, ChangeLanguageActivity.class));
    }

    @OnClick(R.id.txtRegister)
    void register() {
        startActivity(new Intent(this, RegisterActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        preferences = getSharedPreferences("Farmss", Context.MODE_PRIVATE);

        // checks for permissions and shows the dialog for granting or denying a permission.
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_NETWORK_STATE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnLogin.setBackgroundResource(R.drawable.ripple_rounded);
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.login));
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Whitney-Book-Bas.otf");
        etEmail.setTypeface(typeface);
        etPassword.setTypeface(typeface);
        btnLogin.setTypeface(typeface);
        txtForgotPassword.setTypeface(typeface);
        txtRegister.setTypeface(typeface);
        txtChangeLanguage.setTypeface(typeface);
    }

    private void login(final String email, final String password) {

        String tag_str_req = "req_login";
        sessionManager = new SessionManager(getApplicationContext());

        dialog = new MaterialDialog.Builder(LoginActivity.this)
                .title(R.string.progress_dialog_login_title)
                .content(R.string.progress_dialog_login)
                .progress(true, 0)
                .show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, NetworkConstants.LOGIN_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login response: " + response.toString());
                dialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean res = jsonObject.getBoolean("res");

                    if (res) {
                        preferences.edit().putString("userId", jsonObject.getString("user_id")).commit();
                        sessionManager.setLogin(true);
                        Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("login", "login");
                        intent.putExtras(bundle);
                        startActivity(intent);
                        ActivityCompat.finishAffinity(LoginActivity.this);
                    } else {
                        Snackbar.show(LoginActivity.this, getString(R.string.authorization_error));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error response : " + error.getMessage());
                Snackbar.show(LoginActivity.this, getString(R.string.network_error));
                dialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("password", password);
                preferences.edit().putString("login_id", email).commit();
                return params;
            }
        };

        MainApplication.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        ActivityCompat.finishAffinity(this);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED
                        && grantResults[2] == PackageManager.PERMISSION_GRANTED
                        && grantResults[3] == PackageManager.PERMISSION_GRANTED
                        && grantResults[4] == PackageManager.PERMISSION_GRANTED
                        && grantResults[5] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Toast.makeText(this, "Permissions Required!!", Toast.LENGTH_LONG).show();
                    ActivityCompat.finishAffinity(this);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}
