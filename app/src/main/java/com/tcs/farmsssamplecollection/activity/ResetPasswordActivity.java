package com.tcs.farmsssamplecollection.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tcs.farmsssamplecollection.R;
import com.tcs.farmsssamplecollection.constant.NetworkConstants;
import com.tcs.farmsssamplecollection.global.MainApplication;
import com.tcs.farmsssamplecollection.ui.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by harshdeepsingh on 29/03/17.
 */

// activity for resetting the pasword
public class ResetPasswordActivity extends AppCompatActivity {

    @Bind(R.id.etUniqueCode)
    EditText etUniqueCode;
    @Bind(R.id.etNewPassword)
    EditText etNewPassword;
    @Bind(R.id.etConfirmNewPassword)
    EditText etConfirmNewPassword;
    @Bind(R.id.btnResetPassword)
    Button btnResetPassword;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.txtResetPasswordMsg)
    TextView txtResetPasswordMsg;
    private MaterialDialog dialog;
    private static final String TAG = ResetPasswordActivity.class.getSimpleName();

    @OnClick(R.id.btnResetPassword)
    void changePassword() {
        if (etUniqueCode.getText().toString().equals("") || etNewPassword.getText().toString().equals("")) {
            Snackbar.show(ResetPasswordActivity.this, getString(R.string.no_text));
            return;
        } else if(!etNewPassword.getText().toString().equals(etConfirmNewPassword.getText().toString())) {
            Snackbar.show(ResetPasswordActivity.this, getString(R.string.matching_passwords));
            return;
        } else {
            resetPassword(etUniqueCode.getText().toString(), etNewPassword.getText().toString());
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.reset_password));
        txtResetPasswordMsg.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Whitney-Book-Bas.otf"));
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnResetPassword.setBackgroundResource(R.drawable.ripple);
        }
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Whitney-Book-Bas.otf");
        txtResetPasswordMsg.setTypeface(typeface);
        etNewPassword.setTypeface(typeface);
        etConfirmNewPassword.setTypeface(typeface);
        etUniqueCode.setTypeface(typeface);
        btnResetPassword.setTypeface(typeface);
    }

    private void resetPassword(final String code, final String newpass) {

        Intent intent = getIntent();
        final String email = intent.getStringExtra("email");
        Log.e(TAG, email);
        String tag_str_req = "req_new_pass";
        dialog = new MaterialDialog.Builder(ResetPasswordActivity.this)
                .title(R.string.reset_password)
                .content(R.string.password_change)
                .progress(true, 0)
                .show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, NetworkConstants.RESET_PASSWORD_CHANGE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response.toString());
                dialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean res = jsonObject.getBoolean("res");
                    if (res) {
                        Intent intent = new Intent(ResetPasswordActivity.this, LoginActivity.class);
                        startActivity(intent);
                    } else {
                        Snackbar.show(ResetPasswordActivity.this, getString(R.string.invalid_attempt));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.getMessage());
                Snackbar.show(ResetPasswordActivity.this, error.getMessage());
                dialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("code", code);
                params.put("newpass", newpass);
                return params;
            }
        };

        MainApplication.getInstance().addToRequestQueue(stringRequest, tag_str_req);
    }


    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    @Override
    public void onBackPressed() {

    }

}
