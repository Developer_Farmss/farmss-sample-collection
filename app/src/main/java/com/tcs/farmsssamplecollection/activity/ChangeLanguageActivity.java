package com.tcs.farmsssamplecollection.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.akexorcist.localizationactivity.LocalizationActivity;
import com.tcs.farmsssamplecollection.R;
import com.tcs.farmsssamplecollection.util.SessionManager;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by harshdeepsingh on 29/03/17.
 */

// this activity is the display of the change language functionality in the application
public class ChangeLanguageActivity extends LocalizationActivity implements View.OnClickListener{

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.radioGrpLang)
    RadioGroup radioGrpLang;
    @Bind(R.id.radioEnglish)
    RadioButton radioEnglish;
    @Bind(R.id.radioHindi)
    RadioButton radioHindi;
    @Bind(R.id.radioMarathi)
    RadioButton radioMarathi;
    @Bind(R.id.btnChangeLanguage)
    Button btnChangeLanguage;
    private SessionManager sessionManager;
    public Intent intent;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_language);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.changeLanguage));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Whitney-Semibold-Bas.otf");
        radioEnglish.setTypeface(typeface);
        radioHindi.setTypeface(typeface);
        radioMarathi.setTypeface(typeface);
        btnChangeLanguage.setTypeface(typeface);
        sessionManager = new SessionManager(getApplicationContext());
        intent = new Intent(this, LoginActivity.class);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnChangeLanguage.setBackgroundResource(R.drawable.ripple);
        }

        btnChangeLanguage.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (radioEnglish.isChecked()) {
            setLanguage("en");
            sessionManager.setLang("english");
            startActivity(intent);
        } else if (radioHindi.isChecked()) {
            setLanguage("hi");
            sessionManager.setLang("hindi");
            startActivity(intent);
        } else if (radioMarathi.isChecked()) {
            setLanguage("mr");
            sessionManager.setLang("marathi");
            startActivity(intent);
        }
    }
}
