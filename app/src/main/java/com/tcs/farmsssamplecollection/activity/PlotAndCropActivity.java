package com.tcs.farmsssamplecollection.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.tcs.farmsssamplecollection.R;
import com.tcs.farmsssamplecollection.adapter.CropAdapter;
import com.tcs.farmsssamplecollection.database.DBActivity;
import com.tcs.farmsssamplecollection.dto.CropDTO;
import com.tcs.farmsssamplecollection.global.GPSTracker;
import com.tcs.farmsssamplecollection.ui.Snackbar;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by harshdeepsingh on 04/04/17.
 */

// Activity for entering the plot size and crop.
public class PlotAndCropActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.btnAddField)
    Button btnAddField;
    @Bind(R.id.etPlotSize)
    EditText etPlotSize;
    @Bind(R.id.txtCrop)
    TextView txtCrop;
    @Bind(R.id.btnSubmit)
    Button btnSubmit;
    @Bind(R.id.radioGroup)
    RadioGroup radioGroup;
    @Bind(R.id.radioAcre)
    RadioButton radioAcre;
    @Bind(R.id.radioGunthe)
    RadioButton radioGunthe;
    @Bind(R.id.searchCrop)
    android.widget.SearchView searchCrop;
    @Bind(R.id.listCrops)
    ListView listCrops;
    String[] cropNames;
    private ArrayList<CropDTO> crops;
    private CropAdapter adapter;
    MaterialDialog dialog;
    GPSTracker tracker;
    SharedPreferences preferences;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //dialog.dismiss();

        //Getting the location of the service person which in turn will give the farm location
        tracker = new GPSTracker(PlotAndCropActivity.this);
        if (tracker.canGetLocation()) {
            double lat = tracker.getLatitude();
            double lon = tracker.getLongitude();
        } else {
            tracker.showSettingsAlert();
        }
        setContentView(R.layout.activity_plot_and_crop);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        populate();
    }

    private void populate() {

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.plot_and_crop_activity));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Whitney-Book-Bas.otf");
        etPlotSize.setTypeface(typeface);
        radioAcre.setTypeface(typeface);
        radioGunthe.setTypeface(typeface);
        btnAddField.setTypeface(typeface);
        btnSubmit.setTypeface(typeface);
        preferences = getSharedPreferences("Farmss", Context.MODE_PRIVATE);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnAddField.setBackgroundResource(R.drawable.ripple);
        }
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnSubmit.setBackgroundResource(R.drawable.ripple);
        }

        txtCrop.setVisibility(View.INVISIBLE);
        crops = new ArrayList<CropDTO>();

        cropNames = new String[]{getString(R.string.rice), getString(R.string.raabi), getString(R.string.kharif_jowar),
                getString(R.string.wheat), getString(R.string.corn), getString(R.string.finger_millet), getString(R.string.harbor), getString(R.string.split_red_gram),
                getString(R.string.split_black_gram), getString(R.string.split_red_lentil),
                getString(R.string.moth_bean), getString(R.string.kidney_beans), getString(R.string.black_eyed_beans),
                getString(R.string.peanut), getString(R.string.soyabean), getString(R.string.sunflower), getString(R.string.safflower),
                getString(R.string.mole), getString(R.string.sugarcane), getString(R.string.cotton), getString(R.string.oat), getString(R.string.fodder), getString(R.string.garlic),
                getString(R.string.stylo), getString(R.string.mango), getString(R.string.banana), getString(R.string.grapes), getString(R.string.pomegranate), getString(R.string.custard_apple),
                getString(R.string.amla), getString(R.string.fig), getString(R.string.bair), getString(R.string.guava),
                getString(R.string.sapota), getString(R.string.coconut), getString(R.string.blackberry), getString(R.string.tamarind), getString(R.string.lemon),
                getString(R.string.orange), getString(R.string.strawberry), getString(R.string.papaya), getString(R.string.onion), getString(R.string.chilli), getString(R.string.tomato),
                getString(R.string.brinjal), getString(R.string.ladyfinger), getString(R.string.cabbage), getString(R.string.cauliflower), getString(R.string.broccoli), getString(R.string.potato),
                getString(R.string.green_peas), getString(R.string.butter_beans), getString(R.string.turmeric), getString(R.string.ginger), getString(R.string.panvel), getString(R.string.rose),
                getString(R.string.gerbera), getString(R.string.carnation), getString(R.string.tuberose), getString(R.string.gladiolous), getString(R.string.aster), getString(R.string.raddish), getString(R.string.cucumber),
                getString(R.string.bottle_gourd), getString(R.string.bitter_gourd), getString(R.string.ridge_gourd), getString(R.string.water_melon),
                getString(R.string.musk_melon), getString(R.string.fenugreek), getString(R.string.spinach), getString(R.string.cluster_beans),
                getString(R.string.french_beans), getString(R.string.garlic), getString(R.string.sweet_potato), getString(R.string.colocasia), getString(R.string.elephant_foot_yam),
                getString(R.string.sago), getString(R.string.drumstick)
        };

        for (int i = 0; i < cropNames.length; i++) {
            CropDTO cropDTO = new CropDTO(cropNames[i]);
            // Binds all strings into an array
            crops.add(cropDTO);
        }

        adapter = new CropAdapter(PlotAndCropActivity.this, crops);
        listCrops.setAdapter(adapter);
        listCrops.setTextFilterEnabled(true);
        Bundle bundle = getIntent().getBundleExtra("farmer");
        final String farmerName = bundle.getString("farmerName");
        final String farmerMobile = bundle.getString("farmerMobile");
        final String state = bundle.getString("state");
        final String district = bundle.getString("district");
        final String tehsil = bundle.getString("tehsil");
        final String village = bundle.getString("village");
        final String pincode = bundle.getString("pincode");

        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchCrop.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        searchCrop.setIconifiedByDefault(false);
        searchCrop.setQueryHint(getString(R.string.select_crop));
        searchCrop.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.filter(newText);
                return false;
            }
        });

        listCrops.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                searchCrop.setVisibility(View.GONE);
                listCrops.setVisibility(View.GONE);
                txtCrop.setVisibility(View.VISIBLE);
                CropDTO dto = (CropDTO) parent.getAdapter().getItem(position);
                String item = dto.getName();
                txtCrop.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Whitney-Semibold-Bas.otf"));
                txtCrop.setText(item);

            }
        });

        txtCrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchCrop.setVisibility(View.VISIBLE);
                listCrops.setVisibility(View.VISIBLE);
                txtCrop.setVisibility(View.GONE);
            }
        });

        // button click to store the results in local database
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Long addedOn = System.currentTimeMillis()/1000;
                final String ao = addedOn.toString();
                if (!(etPlotSize.getText().toString().equals(""))) {
                    if (!(txtCrop.getText().toString().equals(""))) {
                        String unit = "";
                        if (radioAcre.isChecked()) {
                            unit = radioAcre.getText().toString();
                        } else if (radioGunthe.isChecked()) {
                            unit = radioGunthe.getText().toString();
                        }
                        DBActivity db = new DBActivity(PlotAndCropActivity.this);
                        db.open();
                        db.insertDB(farmerName, farmerMobile, state, district, tehsil, village, pincode,
                                etPlotSize.getText().toString() + " " + unit, txtCrop.getText().toString(),
                                String.valueOf(tracker.getLatitude()),
                                String.valueOf(tracker.getLongitude()), preferences.getString("login_id", null), ao);
                        db.close();
                        Intent intent = new Intent(PlotAndCropActivity.this, DashboardActivity.class);
                        intent.putExtra("farmer added", "success");
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    } else {
                        Snackbar.show(PlotAndCropActivity.this, "Please Select A Crop!!");
                        return;
                    }
                } else {
                    Snackbar.show(PlotAndCropActivity.this, "Please Enter Plot Size!!");
                    return;
                }
            }
        });

        // button click to add new field to the same farmer
        btnAddField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Long addedOn = System.currentTimeMillis()/1000;
                final String ao = addedOn.toString();
                if (!(etPlotSize.getText().toString().equals(""))) {
                    if (!(txtCrop.getText().toString().equals(""))) {
                        dialog = new MaterialDialog.Builder(PlotAndCropActivity.this)
                                .title(R.string.confirmation)
                                .content(R.string.new_plot_alert)
                                .positiveText(R.string.yes)
                                .negativeText(R.string.no)
                                .typeface(Typeface.createFromAsset(getAssets(), "fonts/Whitney-Semibold-Bas.otf"),
                                        Typeface.createFromAsset(getAssets(), "fonts/Whitney-Semibold-Bas.otf"))
                                .callback(new MaterialDialog.ButtonCallback() {
                                    @Override
                                    public void onPositive(MaterialDialog dialog) {
                                        String unit = "";
                                        if (radioAcre.isChecked()) {
                                            unit = radioAcre.getText().toString();
                                        } else if (radioGunthe.isChecked()) {
                                            unit = radioGunthe.getText().toString();
                                        }
                                        DBActivity db = new DBActivity(PlotAndCropActivity.this);
                                        db.open();
                                        db.insertDB(farmerName, farmerMobile, state, district, tehsil, village, pincode,
                                                etPlotSize.getText().toString() + " " + unit, txtCrop.getText().toString(),
                                                String.valueOf(tracker.getLatitude()), String.valueOf(tracker.getLongitude()),
                                                preferences.getString("login_id", null), ao);
                                        db.close();
                                        Bundle bundle = new Bundle();
                                        bundle.putString("farmerName", farmerName);
                                        bundle.putString("farmerMobile", farmerMobile);
                                        bundle.putString("state", state);
                                        bundle.putString("district", district);
                                        bundle.putString("tehsil", tehsil);
                                        bundle.putString("village", village);
                                        bundle.putString("pincode", pincode);
                                        Intent intent = new Intent(PlotAndCropActivity.this, PlotAndCropActivity.class);
                                        intent.putExtra("farmer", bundle);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                        Toast.makeText(PlotAndCropActivity.this, R.string.saved_previous, Toast.LENGTH_SHORT).show();
                                        super.onPositive(dialog);
                                    }

                                    @Override
                                    public void onNegative(MaterialDialog dialog) {
                                        dialog.dismiss();
                                        super.onNegative(dialog);
                                    }
                                }).show();

                    } else {
                        Snackbar.show(PlotAndCropActivity.this, getString(R.string.crop_alert));
                        return;
                    }
                } else {
                    Snackbar.show(PlotAndCropActivity.this, getString(R.string.plot_size_alert));
                    return;
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
