package com.tcs.farmsssamplecollection.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.tcs.farmsssamplecollection.R;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by harshdeepsingh on 03/05/17.
 */

// This activity just displays the farmer details as they were stored by the service person.
public class FarmerProfileActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.txtFarmerName)
    TextView txtFarmerName;
    @Bind(R.id.txtFarmerMobile)
    TextView txtFarmerMobile;
    @Bind(R.id.txtFarmerMobileValue)
    TextView txtFarmerMobileValue;
    @Bind(R.id.txtFarmerVillage)
    TextView txtFarmerVillage;
    @Bind(R.id.txtFarmerVillageValue)
    TextView txtFarmerVillageValue;
    @Bind(R.id.txtFarmerTehsil)
    TextView txtFarmerTehsil;
    @Bind(R.id.txtFarmerTehsilValue)
    TextView txtFarmerTehsilValue;
    @Bind(R.id.txtFarmerDistrict)
    TextView txtFarmerDistrict;
    @Bind(R.id.txtFarmerDistrictValue)
    TextView txtFarmerDistrictValue;
    @Bind(R.id.txtFarmerState)
    TextView txtFarmerState;
    @Bind(R.id.txtFarmerStateValue)
    TextView txtFarmerStateValue;
    @Bind(R.id.txtFarmerPincode)
    TextView txtFarmerPincode;
    @Bind(R.id.txtFarmerPincodeValue)
    TextView txtFarmerPincodeValue;
    @Bind(R.id.txtFarmerPlotSize)
    TextView txtFarmerPlotSize;
    @Bind(R.id.txtFarmerPlotSizeValue)
    TextView txtFarmerPlotSizeValue;
    @Bind(R.id.txtFarmerCrop)
    TextView txtFarmerCrop;
    @Bind(R.id.txtFarmerCropValue)
    TextView txtFarmerCropValue;
    @Bind(R.id.txtFarmerID)
    TextView txtFarmerID;
    @Bind(R.id.txtFarmerIDValue)
    TextView txtFarmerIDValue;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmer_profile);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.farmer_details));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Whitney-Book-Bas.otf");
        txtFarmerCrop.setTypeface(typeface);
        txtFarmerCropValue.setTypeface(typeface);
        txtFarmerDistrict.setTypeface(typeface);
        txtFarmerDistrictValue.setTypeface(typeface);
        txtFarmerMobile.setTypeface(typeface);
        txtFarmerMobileValue.setTypeface(typeface);
        txtFarmerName.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Whitney-Semibold-Bas.otf"));
        txtFarmerPincode.setTypeface(typeface);
        txtFarmerPincodeValue.setTypeface(typeface);
        txtFarmerPlotSize.setTypeface(typeface);
        txtFarmerPlotSizeValue.setTypeface(typeface);
        txtFarmerState.setTypeface(typeface);
        txtFarmerStateValue.setTypeface(typeface);
        txtFarmerTehsil.setTypeface(typeface);
        txtFarmerTehsilValue.setTypeface(typeface);
        txtFarmerVillage.setTypeface(typeface);
        txtFarmerVillageValue.setTypeface(typeface);
        txtFarmerID.setTypeface(typeface);
        txtFarmerIDValue.setTypeface(typeface);

        try {
            JSONObject jsonObject = new JSONObject(getIntent().getStringExtra("user"));
            txtFarmerName.setText(jsonObject.getString("name"));
            txtFarmerMobileValue.setText(jsonObject.getString("mobile"));
            txtFarmerStateValue.setText(jsonObject.getString("state"));
            txtFarmerDistrictValue.setText(jsonObject.getString("district"));
            txtFarmerTehsilValue.setText(jsonObject.getString("tehsil"));
            txtFarmerVillageValue.setText(jsonObject.getString("village"));
            txtFarmerPincodeValue.setText(jsonObject.getString("pincode"));
            txtFarmerCropValue.setText(jsonObject.getString("crop"));
            txtFarmerPlotSizeValue.setText(jsonObject.getString("plotSize"));
            txtFarmerIDValue.setText(jsonObject.getString("unique_id"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_farmer_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        if (id == R.id.menuAdd) {
            Bundle bundle = new Bundle();
            bundle.putString("farmerName", txtFarmerName.getText().toString());
            bundle.putString("farmerMobile", txtFarmerMobileValue.getText().toString());
            bundle.putString("state", txtFarmerStateValue.getText().toString());
            bundle.putString("district", txtFarmerDistrictValue.getText().toString());
            bundle.putString("tehsil", txtFarmerTehsilValue.getText().toString());
            bundle.putString("village", txtFarmerVillageValue.getText().toString());
            bundle.putString("pincode", txtFarmerPincodeValue.getText().toString());
            Intent intent = new Intent(FarmerProfileActivity.this, PlotAndCropActivity.class);
            intent.putExtra("farmer", bundle);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
