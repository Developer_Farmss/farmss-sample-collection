package com.tcs.farmsssamplecollection.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.akexorcist.localizationactivity.LocalizationActivity;
import com.tcs.farmsssamplecollection.R;
import com.tcs.farmsssamplecollection.global.GPSTracker;
import com.tcs.farmsssamplecollection.util.SessionManager;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by harshdeepsingh on 29/03/17.
 */

// welcome activity
public class SplashActivity extends LocalizationActivity {

    private SessionManager sessionManager;
    GPSTracker tracker;
    @Bind(R.id.txtWelcomeMsgMr)
    TextView txtWelcomeMsg;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        sessionManager = new SessionManager(getApplicationContext());
        txtWelcomeMsg.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Whitney-Semibold-Bas.otf"));

        Thread thread = new Thread() {
            public void run() {
                try {
                    if (sessionManager.checkLang().equals("english")) {
                        setLanguage("en");
                    } else if (sessionManager.checkLang().equals("hindi")) {
                        setLanguage("hi");
                    } else if (sessionManager.checkLang().equals("marathi")) {
                        setLanguage("mr");
                    }
                    sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    if (sessionManager.isLoggedIn()) {
                        startActivity(new Intent(SplashActivity.this, DashboardActivity.class));
                    } else {
                        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    }
                }
            }
        };
        thread.start();
    }


    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    @Override
    public void onBackPressed() {
    }

}
