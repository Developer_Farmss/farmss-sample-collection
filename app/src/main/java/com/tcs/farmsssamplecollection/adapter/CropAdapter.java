package com.tcs.farmsssamplecollection.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tcs.farmsssamplecollection.R;
import com.tcs.farmsssamplecollection.dto.CropDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by harshdeepsingh on 05/04/17.
 */

// Adapter for listview in the crops
public class CropAdapter extends BaseAdapter {
    public Context context;
    private List<CropDTO> crops = null;
    private ArrayList<CropDTO> arrayList;

    public CropAdapter(Context context, List<CropDTO> crops) {
        super();
        this.context = context;
        this.crops = crops;
        this.arrayList = new ArrayList<CropDTO>();
        this.arrayList.addAll(crops);
    }

    public class CropsHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return crops.size();
    }

    @Override
    public Object getItem(int position) {
        return crops.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CropsHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.crop_custom_row, parent, false);
            holder = new CropsHolder();
            holder.name = (TextView) convertView.findViewById(R.id.txtCrop);
            holder.name.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Whitney-Book-Bas.otf"));
            convertView.setTag(holder);
        } else {
            holder = (CropsHolder) convertView.getTag();
        }
        holder.name.setText(crops.get(position).getName());

        return convertView;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        crops.clear();
        if (charText.length() == 0) {
            crops.addAll(arrayList);
        } else {
            for (CropDTO wp : arrayList) {
                if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    crops.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}