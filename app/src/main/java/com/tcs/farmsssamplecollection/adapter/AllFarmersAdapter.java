package com.tcs.farmsssamplecollection.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tcs.farmsssamplecollection.R;
import com.tcs.farmsssamplecollection.dto.AllFarmersDTO;

import java.util.List;

/**
 * Created by harshdeepsingh on 21/06/17.
 */

public class AllFarmersAdapter extends RecyclerView.Adapter<AllFarmersAdapter.FarmerViewHolder> {

    private LayoutInflater inflater;
    private Context context;
    private List<AllFarmersDTO> allFarmersDTOList;

    public AllFarmersAdapter(Context context, List<AllFarmersDTO> allFarmersDTOList, OnItemClickListener listener) {
        this.context = context;
        this.allFarmersDTOList = allFarmersDTOList;
        this.listener = listener;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public FarmerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.farmer_list_custom_row, parent, false);
        FarmerViewHolder holder = new FarmerViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(FarmerViewHolder holder, int position) {
        holder.bind(allFarmersDTOList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return allFarmersDTOList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public interface OnItemClickListener {
        void onItemClick(AllFarmersDTO allFarmersDTO);
    }

    private OnItemClickListener listener;

    class FarmerViewHolder extends RecyclerView.ViewHolder {
        TextView txtName, txtId, txtMobile, txtVillage, txtPlot, txtCrop;

        public FarmerViewHolder(View itemView) {
            super(itemView);

            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtId = (TextView) itemView.findViewById(R.id.txtId);
            txtMobile = (TextView) itemView.findViewById(R.id.txtMobile);
            txtVillage = (TextView) itemView.findViewById(R.id.txtAddress);
            txtPlot = (TextView) itemView.findViewById(R.id.txtPlotsize);
            txtCrop = (TextView) itemView.findViewById(R.id.txtCropDashboard);
        }

        public void bind(final AllFarmersDTO allFarmersDTO, final OnItemClickListener listener) {
            txtName.setText(allFarmersDTO.getName());
            txtId.setText(allFarmersDTO.getId());
            txtMobile.setText(allFarmersDTO.getMobile());
            txtPlot.setText(allFarmersDTO.getPlotsize());
            txtVillage.setText(allFarmersDTO.getVillage());
            txtCrop.setText(allFarmersDTO.getCrop());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(allFarmersDTO);
                }
            });
        }
    }
}
