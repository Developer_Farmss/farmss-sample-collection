package com.tcs.farmsssamplecollection.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tcs.farmsssamplecollection.R;
import com.tcs.farmsssamplecollection.dto.FarmerDTO;

import java.util.Collections;
import java.util.List;

/**
 * Created by harshdeepsingh on 29/03/17.
 */

// Adapter for the recyler view used in the dashboard
public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.DashboardViewHolder> {
    private LayoutInflater inflater;
    List<FarmerDTO> farmerDTOList = Collections.emptyList();
    private Context context;

    public interface OnItemClickListener {
        void onItemClick(FarmerDTO item);
    }

    //    private List<FarmerDTO> items;
    private OnItemClickListener listener;

    public DashboardAdapter(Context context, List<FarmerDTO> farmerDTOList, OnItemClickListener listener) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.farmerDTOList = farmerDTOList;
        this.listener = listener;
    }


    @Override
    public DashboardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.dashboard_custom_row, parent, false);
        DashboardViewHolder holder = new DashboardViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(DashboardViewHolder holder, int position) {
        holder.bind(farmerDTOList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return farmerDTOList.size();
    }

    public void setFilter(List<FarmerDTO> list) {
        farmerDTOList = list;
        notifyDataSetChanged();
    }

    class DashboardViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout rlFarmerCard;
        TextView txtName;
        TextView txtMobile;
        TextView txtVillage, txtPlotSize, txtCropDashboard, txtMobileLabel, txtVillageLabel, txtPlotSizeLabel, txtCropLabel, txtAddedONLabel, txtAddedOn;

        public DashboardViewHolder(View itemView) {
            super(itemView);

            rlFarmerCard = (RelativeLayout) itemView.findViewById(R.id.rlFarmerCard);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtMobile = (TextView) itemView.findViewById(R.id.txtMobile);
            txtVillage = (TextView) itemView.findViewById(R.id.txtAddress);
            txtPlotSize = (TextView) itemView.findViewById(R.id.txtPlotsize);
            txtCropDashboard = (TextView) itemView.findViewById(R.id.txtCropDashboard);
            txtMobileLabel = (TextView) itemView.findViewById(R.id.txtMobileLabel);
            txtVillageLabel = (TextView) itemView.findViewById(R.id.txtAddressLabel);
            txtPlotSizeLabel = (TextView) itemView.findViewById(R.id.txtPlotsizeLabel);
            txtCropLabel = (TextView) itemView.findViewById(R.id.txtCropDashboardLabel);
            txtAddedONLabel = (TextView) itemView.findViewById(R.id.txtAddedLabel);
            txtAddedOn = (TextView) itemView.findViewById(R.id.txtAddedValue);
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Whitney-Book-Bas.otf");
            txtName.setTypeface(typeface);
            txtVillage.setTypeface(typeface);
            txtMobile.setTypeface(typeface);
            txtPlotSize.setTypeface(typeface);
            txtCropDashboard.setTypeface(typeface);
            txtMobileLabel.setTypeface(typeface);
            txtVillageLabel.setTypeface(typeface);
            txtPlotSizeLabel.setTypeface(typeface);
            txtCropLabel.setTypeface(typeface);
            txtAddedOn.setTypeface(typeface);
            txtAddedONLabel.setTypeface(typeface);

        }

        public void bind(final FarmerDTO farmerDTO, final OnItemClickListener listener) {
            txtName.setText(farmerDTO.name);
            txtMobile.setText(farmerDTO.mobile);
            txtVillage.setText(farmerDTO.village);
            txtPlotSize.setText(farmerDTO.plotsize);
            txtCropDashboard.setText(farmerDTO.crop);
            txtAddedOn.setText(farmerDTO.addedOn);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(farmerDTO);
                }
            });
        }
    }
}